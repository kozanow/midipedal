#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/cpufunc.h>

typedef unsigned char uchar;

typedef enum
{
	ENotes_C  = 0,
	ENotes_Cs = 1,
	ENotes_D  = 2,
	ENotes_Ds = 3,
	ENotes_E  = 4,
	ENotes_F  = 5,
	ENotes_Fs = 6,
	ENotes_G  = 7,
	ENotes_Gs = 8,
	ENotes_A  = 9,
	ENotes_As = 10,
	ENotes_B  = 11,
	ENotes_C2 = 12,
	ENotes_Size = 13
} ENotes;

typedef struct
{
	uchar b;
	uchar c;
	uchar d;
} Ports;

Ports stablePorts = {0, 0, 0};
Ports changedPorts = {0, 0, 0};

#define ALL_KEYS_COUNT 17
uchar keysDebounceTimers[ALL_KEYS_COUNT];

int8_t noteOnLastOctave[ENotes_Size] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

#define FIRST_ELEMENT_PORT_B 0
#define FIRST_ELEMENT_PORT_C 6
#define FIRST_ELEMENT_PORT_D 10

#define DEBOUNCE_PERIOD_KEYS 30
#define DEBOUNCE_PERIOD_FOOTSWITCH 100

#define IGNORED_PINS_B 0xC0 // 11000000
#define IGNORED_PINS_C 0xF0 // 11110000
#define IGNORED_PINS_D 0x02 // 00000010

#define C4 60
#define MIDI_NOTE_MASK 	   0x7F
#define MIDI_FULL_VELOCITY 0x7F
#define MIDI_NOTE_OFF_CH1  0x80
#define MIDI_NOTE_ON_CH1   0x90

int8_t currentOctave = 0;

uchar debounceFlag = 0;

void configureUart()
{
	// FOSC / 16 / BAUDRATE - 1;
	UBRR0L = 31;
	// start bit / 8 data bits / stop bit
	UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);
	// enable TX
	UCSR0B = (1 << TXEN0);
}

void configurePorts()
{
	DDRB = 	0x00;
    PORTB = 0x00;
    DDRC = 	(1 << PIN4) | (1 << PIN5);
    PORTC = 0x00;
    DDRD = 	0x00;
    PORTD = 0x00;
    _NOP();
    PORTB = (uchar)~IGNORED_PINS_B;
    PORTC = (uchar)~IGNORED_PINS_C;
    PORTD = (uchar)~IGNORED_PINS_D;
}

void configureTimer0()
{
	/*
		counter * prescaler = 16 * 1024
		1024us == 1.024ms @ Fclk = 16MHz
	*/

	// counter max
	OCR0A = 0x0F;
	// Reset counter
	TCNT0 = 0;
	// Enable CTC
	TCCR0A = (1 << WGM01);
	// Start with prescaler = 1024
	TCCR0B = (1 << CS02) | (1 << CS00);
	// Compare match A interrupt
	TIMSK0 = (1 << OCIE0A);
}

void sendUart(const uchar data)
{
	while (!( UCSR0A & (1 << UDRE0))) {}
	UDR0 = data;
}

void updateOctaveLed()
{
	static uint8_t ledClock = 0;

	switch (currentOctave)
	{
		case -3:
			if ((ledClock & 0x7F) == 0) PORTC ^= 0x10;
			break;
		case -2:
			if (ledClock == 0) PORTC ^= 0x10;
			break;
		case -1:
			PORTC |= 0x10;
			break;
		case 0:
			PORTC &= 0x0F;
			break;
		case 1:
			PORTC |= 0x20;
			break;
		case 2:
			if (ledClock == 0) PORTC ^= 0x20;
			break;
		case 3:
			if ((ledClock & 0x7F) == 0) PORTC ^= 0x20;
			break;
	}

	ledClock++;
}

uint8_t getDebouncePeriod(const uint8_t keyIdx)
{
	return (keyIdx >= FIRST_ELEMENT_PORT_C) && (keyIdx < FIRST_ELEMENT_PORT_D) ?
			DEBOUNCE_PERIOD_FOOTSWITCH :
			DEBOUNCE_PERIOD_KEYS;
}

void debouncePort(
		uchar currentPins,
		uchar* stablePins,
		uchar* changedPins,
		const uchar ignoredPins,
		uchar keyIdx)
{
	currentPins &= ~ignoredPins;
	const uchar momentaryChangedPins = currentPins ^ (*stablePins);
	for (uchar pinMask = 0x01; pinMask; pinMask <<= 1)
	{
		if ((pinMask & ignoredPins) == 0)
		{
			if ((momentaryChangedPins & pinMask) == 0)
			{
				// current pin is different than stable pin
				keysDebounceTimers[keyIdx]++;
			}
			else
			{
				keysDebounceTimers[keyIdx] = 0;
			}

			if (keysDebounceTimers[keyIdx] == getDebouncePeriod(keyIdx))
			{
				(*changedPins) |= pinMask;
				(*stablePins) ^= pinMask;
				keysDebounceTimers[keyIdx] = 0;
			}

			keyIdx++;
		}
	}
}

void debounce()
{
	debouncePort(PINB, &stablePorts.b, &changedPorts.b, IGNORED_PINS_B, FIRST_ELEMENT_PORT_B);
	debouncePort(PINC, &stablePorts.c, &changedPorts.c, IGNORED_PINS_C, FIRST_ELEMENT_PORT_C);
	debouncePort(PIND, &stablePorts.d, &changedPorts.d, IGNORED_PINS_D, FIRST_ELEMENT_PORT_D);
}

void sendNoteCommand(
		const ENotes note,
		const uchar cmd,
		const int8_t octave)
{
	sendUart(cmd);
	const int8_t midiNote = (C4 + (octave * 12) + note) & MIDI_NOTE_MASK;
	sendUart(midiNote);
	sendUart(MIDI_FULL_VELOCITY);
}

void noteHandler(
		const ENotes note,
		const uchar state)
{
	if (currentOctave != noteOnLastOctave[note])
	{
		sendNoteCommand(note, MIDI_NOTE_OFF_CH1, noteOnLastOctave[note]);
	}

	if (state)
	{
		sendNoteCommand(note, MIDI_NOTE_ON_CH1, currentOctave);
		noteOnLastOctave[note] = currentOctave;
	}
	else
	{
		sendNoteCommand(note, MIDI_NOTE_OFF_CH1, currentOctave);
	}
}

void octaveUp()
{
	if (currentOctave < 3)
	{
		currentOctave++;
	}
}

void octaveDown()
{
	if (currentOctave > -3)
	{
		currentOctave--;
	}
}

void programUp()
{
	// TODO:
}

void programDown()
{
	// TODO:
}

void pinChangeHandlerB(
		const uchar pinMask,
		const uchar stablePins)
{
	const uchar pinState = pinMask & stablePins;
	switch (pinMask)
	{
		case 0x01: noteHandler(ENotes_G, 	pinState); break;
		case 0x02: noteHandler(ENotes_Gs, 	pinState); break;
		case 0x04: noteHandler(ENotes_A, 	pinState); break;
		case 0x08: noteHandler(ENotes_As, 	pinState); break;
		case 0x10: noteHandler(ENotes_B, 	pinState); break;
		case 0x20: noteHandler(ENotes_C2, 	pinState); break;
	}
}

void pinChangeHandlerC(
		const uchar pinMask,
		const uchar stablePins)
{
	const uchar pinState = pinMask & stablePins;
	switch (pinState)
	{
		case 0x04: octaveDown(); break;
		case 0x08: octaveUp(); break;
		case 0x01: octaveDown(); break;
		case 0x02: octaveUp(); break;
	}
}

void pinChangeHandlerD(
		const uchar pinMask,
		const uchar stablePins)
{
	const uchar pinState = pinMask & stablePins;
	switch (pinMask)
	{
		case 0x01: noteHandler(ENotes_C,	pinState); break;
		case 0x02: break; // used for UART TX
		case 0x04: noteHandler(ENotes_Cs,	pinState); break;
		case 0x08: noteHandler(ENotes_D,	pinState); break;
		case 0x10: noteHandler(ENotes_Ds,	pinState); break;
		case 0x20: noteHandler(ENotes_E,	pinState); break;
		case 0x40: noteHandler(ENotes_F,	pinState); break;
		case 0x80: noteHandler(ENotes_Fs,	pinState); break;
	}
}

typedef void (*PinChangeHandler)(const uchar, const  uchar);

void handlePinChange(
		uchar* changedPins,
		const uchar stablePins,
		const uchar ignoredPins,
		PinChangeHandler pinChangeHandler)
{
	for (uchar pinMask = 0x01; pinMask; pinMask <<= 1)
	{
		if ((pinMask & ignoredPins) == 0 && (pinMask & (*changedPins)) != 0)
		{
			pinChangeHandler(pinMask, stablePins);
		}
	}

	(*changedPins) = 0;
}

ISR(TIMER0_COMPA_vect)
{
	updateOctaveLed();
	debounceFlag = 1;
}

int main(void)
{
	cli();
	configureTimer0();
	configureUart();
	configurePorts();
    sei();

    while (1)
    {
    	if (debounceFlag)
    	{
    		debounceFlag = 0;
    		debounce();
    	}

		if (changedPorts.b)
		{
			handlePinChange(&changedPorts.b, stablePorts.b, IGNORED_PINS_B, pinChangeHandlerB);
		}
		if (changedPorts.c)
		{
			handlePinChange(&changedPorts.c, stablePorts.c, IGNORED_PINS_C, pinChangeHandlerC);
		}
		if (changedPorts.d)
		{
			handlePinChange(&changedPorts.d, stablePorts.d, IGNORED_PINS_D, pinChangeHandlerD);
		}
    }

    return (1);	// should never happen
}
